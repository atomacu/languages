@extends('layouts.guest')

@section('content')
<section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(images/big_image_1.jpg);">
    <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
            <div class="col-md-8 text-center">

                <div class="mb-5 element-animate">
                    <h1>Learn From Doing</h1>
                    <p class="lead">Learn something new every day with skwela lorem ipsum dolor sit amet.</p>
                    <p><a href="#" class="btn btn-primary">Sign up and get a 7-day free trial</a></p>
                </div>


            </div>
        </div>
    </div>
</section>
<div class="container">
        <div class="py-4 row">
            @foreach ($classes as $class)
            <div class="mb-3 col-md-3">
                <div class="card card-product">
                    <div class="img-wrap">
                        <a href="{{route('clientClasses.show',['id'=>$class->id])}}"><img src="{{ asset('/images/classes/'.$class->img)}}"></a>
                    </div>
                    <div class="m-2 info-wrap">
                        <h4 class="title">{{$class->name}}</h4>
                        <p class="desc text-truncate">{{$class->description}}</p>
                    </div>
                    <div class="m-2 bottom-wrap">
                        <div class="container">
                            <div class="row">
                                @if($class->exist==0)    
                                    <button data-auth="{{Auth::user()? 1 : 0}}" data-id="{{$class->id}}" data-url="{{route('wishlist.store')}}" id="add-class-wishlist" class="btn ml-1 my-1 col-md btn-sm btn-danger">
                                        <i class="fas fa-heart"></i> Wishlist
                                    </button>
                                @endif
                                <a href="{{route('clientClasses.show',['id'=>$class->id])}}"
                                    class="btn ml-1 my-1 col-md btn-sm btn-primary">
                                    <i class="fas fa-info"></i> More details
                                </a>
                                <a href="" class="btn ml-1 col-md my-1 btn-sm btn-success">
                                    <i class="fas fa-shopping-cart"></i> Buy
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            @endforeach
             
        </div>
    </div>
<section id="wellcome-our-services" class="school-features d-flex">
    <div class="container">
        <div class="inner">
            <div class="media d-block feature">
                <div class="icon"><span class="flaticon-video-call"></span></div>
                <div class="media-body">
                    <h3 class="mt-0">Online trainings from experts</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora fuga suscipit numquam esse
                        saepe quam, eveniet iure assumenda dignissimos aliquam!</p>
                </div>
            </div>

            <div class="media d-block feature">
                <div class="icon"><span class="flaticon-student"></span></div>
                <div class="media-body">
                    <h3 class="mt-0">Learn anywhere in the world</h3>
                    <p>Delectus fuga voluptatum minus amet, mollitia distinctio assumenda voluptate quas repellat eius
                        quisquam odio. Aliquam, laudantium, optio? Error velit, alias.</p>
                </div>
            </div>

            <div class="media d-block feature">
                <div class="icon"><span class="flaticon-video-player-1"></span></div>
                <div class="media-body">
                    <h3 class="mt-0">Creative learning video</h3>
                    <p>Delectus fuga voluptatum minus amet, mollitia distinctio assumenda voluptate quas repellat eius
                        quisquam odio. Aliquam, laudantium, optio? Error velit, alias.</p>
                </div>
            </div>


            <div class="media d-block feature">
                <div class="icon"><span class="flaticon-audiobook"></span></div>
                <div class="media-body">
                    <h3 class="mt-0">Audio learning</h3>
                    <p>Harum, adipisci, aspernatur. Vero repudiandae quos ab debitis, fugiat culpa obcaecati,
                        voluptatibus ad distinctio cum soluta fugit sed animi eaque?</p>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container">
        <div class="py-4 row">
            @foreach ($categories as $category)  
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('/images/categories/'.$category->img)}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$category->name}}</h5>
                            <p class="card-text text-truncate">{{$category->description}}</p>
                            <div class="row justify-content-center">
                                <div class="col-md-5">
                                    <a href="{{route('clientCategories.show',['id'=>$category->id])}}" class="btn col-md-12 btn-sm btn-primary">See all courses</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            @endforeach  
        </div>
        <div class="row mb-3 justify-content-center">
            <div class="col-md-2">
                <a href="{{route('clientCategories.index')}}" class="btn btn-success">Show all categories</a>
            </div>
        </div>
    </div>


@endsection
