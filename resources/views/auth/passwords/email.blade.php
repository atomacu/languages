@extends('layouts.guest')

@section('content')
<div class="vh-100 container">
    <div class="row my-5 py-5 justify-content-center">
        <div class="py-5 my-5 col-md-8">
            
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row justify-content-center">
                           
                            <div class="col-md-6">
                                <input id="email" placeholder="E-Mail Address" type="email" class="text-center form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0">
                            <div class="col-md-6">
                                <button type="submit" class="btn col-md-12 btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
               
        </div>
    </div>
</div>
@endsection
