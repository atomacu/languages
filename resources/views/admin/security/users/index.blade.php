@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3">
                <i class="fas fa-users"></i> Users
            </p>
        </div>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $i=>$user)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{ucfirst(trans($user->userRole->role->name))}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
