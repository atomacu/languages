@extends('layouts.admin')

@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="row mb-3 justify-content-center">
                            <div class="col-md">
                                <input type="text" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="row mb-3 justify-content-center">
                            <div class="col-md">
                                <textarea type="text" class="form-control" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="row mb-3 justify-content-center">
                            <div class="col-md">
                              <select name="" class="form-control" id="">
                                  <option value=""></option>
                                  <option value=""></option>
                                  <option value=""></option>
                              </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3">
                    <i class="fas fa-user-tag"></i> Roles
            </p>
        </div>
        <div class="col-md-5 ml-auto">
            <button data-toggle="modal" data-target="#exampleModal" class="btn float-right btn-primary btn-sm"><i class="fas fa-plus"></i> Add role</button>
        </div>
    </div>
    <hr>
    <div class="row">
        @foreach ($roles as $role)
            <div class="col-md-4">
                <div class="card">
                    <h5 class="card-header">{{$role->name}}</h5>
                    <div class="card-body">
                        <h5 class="card-title">{{$role->default_page}}</h5>
                        <p class="card-text">{{$role->description}}</p>
                        <hr>
                        <div class="row">
                            <div class="col-md">
                                <div class="custom-control">
                                    <input type="radio" class="custom-control-input" id="default-role-{{$role->id}}" name="radio-stacked" @if($role->set_default==1) checked @endif required>
                                    <label class="custom-control-label" for="default-role-{{$role->id}}"> Default role for new registered users</label>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        @endforeach
    </div>
</div>
@endsection