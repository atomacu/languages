@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3">
                    <i class="fas fa-file-word"></i> Pages
            </p>
        </div>
    </div>
    <hr>
    <div class="row">
        @foreach ($pages as $page)
        <div class="col-md-4 mb-2">
            <div class="card">
                <h5 class="card-header">
                    <i class="far fa-edit"></i> {{$page->name}}
                </h5>
                <div class="card-body">
                    <p class="card-text">
                        <i class="far fa-edit"></i> {{$page->description}}
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
