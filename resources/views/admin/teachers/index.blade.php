@extends('layouts.admin')

@section('content')
<div class="modal fade" id="add-teacher" tabindex="-1" role="dialog" aria-labelledby="add-teacher-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-teacher-label">Add teacher</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form data-url="{{route('teachers.store')}}" id="add-teacher-form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-2 row justify-content-center">
                        <div class="col-md-10">
                            <input id="add-teacher-name" placeholder="Name" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="mb-2 row justify-content-center">
                        <div class="col-md-10">
                            <textarea id="add-teacher-description" placeholder="Description" type="text" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <input type="file" class="form-control" name="teacher-img" id="" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="add-teacher-btn" type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3"><i class="fas fa-address-book"></i> Teachers</p>
        </div>
        <div class="ml-auto col-md-2">
            <button type="button" data-toggle="modal" data-target="#add-teacher" class="btn btn-primary btn-sm ml-auto">
                <i class="fas fa-plus"></i> Add teacher
            </button>
        </div>
    </div>
    <hr>
    <div id="teachers" class="row">
        @foreach($teachers->reverse() as $teacher)
            <div class="mb-2 col-md-3">
                <div class="card">
                    <a data-toggle="modal" data-target="#profile-teacher-{{$teacher->id}}"><img class="teacher-card-img card-img-top" src="{{ asset('/images/teachers/'.$teacher->img)}}" alt="Card image cap"></a>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <a data-toggle="modal" data-target="#profile-teacher-{{$teacher->id}}"> 
                                    <h4 data-id="{{$teacher->id}}" class="teacher-title card-title">{{$teacher->name}}</h4>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="edit-teacher-button text-center btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-teacher-{{$teacher->id}}">
                                        <i class="far fa-edit"></i>
                                </button>
                                <div class="modal fade" id="edit-teacher-{{$teacher->id}}" tabindex="-1" role="dialog" aria-labelledby="edit-teacher-{{$teacher->id}}-label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="edit-teacher-{{$teacher->id}}-label">Edit teacher</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-2 row justify-content-center">
                                                    <div class="col-md-10">
                                                        <input data-id="{{$teacher->id}}" placeholder="Name" value="{{$teacher->name}}" type="text" class="edit-teacher-name form-control">
                                                    </div>
                                                </div>
                                                <div class="mb-2 row justify-content-center">
                                                    <div class="col-md-10">
                                                        <textarea data-id="{{$teacher->id}}" placeholder="Description" class="edit-teacher-description form-control">{{$teacher->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row justify-content-center">
                                                    <div class="col-md-10">
                                                        <img style="width:100%;" src="{{ asset('/images/teachers/'.$teacher->img)}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button data-url="{{route('teachers.update',['id'=>$teacher->id])}}" data-id="{{$teacher->id}}" type="button" class="edit-teacher-btn btn btn-primary">Edit</button>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-10 ">
                                <a data-toggle="modal" data-target="#profile-teacher-{{$teacher->id}}">      
                                    <p data-id="{{$teacher->id}}" class="teacher-description text-truncate card-text">{{$teacher->description}}</p>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <button type="button" data-toggle="modal" data-target="#delete-teacher" class="btn btn-danger mr-auto btn-sm">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                                
                                <div class="modal fade" id="delete-teacher" tabindex="-1" role="dialog" aria-labelledby="delete-teacher-label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="delete-teacher-label">Delete teacher</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            
                                            <div class="modal-body text-center">
                                                <p class="h5">Are you sure you want to delete <b>{{$teacher->name}}</b> ? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button data-url="{{route('teachers.destroy',['id'=>$teacher->id])}}" type="button" class="btn delete-teacher-btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>





                    <div class="modal fade" id="profile-teacher-{{$teacher->id}}" tabindex="-1" role="dialog" aria-labelledby="profile-teacher-{{$teacher->id}}-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="profile-teacher-{{$teacher->id}}-label">profile teacher</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="teacher-profile modal-body">
                                        <div class="teacher-profile card">
                                            <img class="card-img-top" src="{{ asset('/images/teachers/'.$teacher->img)}}" style="width:100%;">
                                            <div class="card-body">
                                                <h5 class="card-title">{{$teacher->name}}</h5>
                                                <p class="card-text">{{$teacher->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>                                            
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-4">
            {{ $teachers->onEachSide(0)->links() }}
        </div>
    </div>
</div>
@endsection
