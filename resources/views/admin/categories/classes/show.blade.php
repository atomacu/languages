
<?php
    if(Auth::user())
   $ext= "admin" ;
    else $ext="guest";
?>
@extends('layouts.'.$ext)


@section('content')
    <div class=" @if(!Auth::user()) vh-100 pt-5  mt-5 @endif  container">
        <div class="row @if(!Auth::user())  mt-5 @endif">
           <div id="class-image-show" class="col-md-5">
               <img src="{{ asset('/images/classes/'.$class->img)}}" alt="">
           </div>
           <div class="col-md-7">
                <div class="row"> 
                    <div class="col-md-10">
                        <h3>
                            {{$class->name}}
                            <small class="text-muted">{{$class->teacher->name}}</small>
                        </h3>
                    </div>
                </div>
                <hr class="mt-0">
                <div class="row">
                    <div class="ml-2 col-md-11">
                        <div class="row">
                            <div class="col-md-3"><p class="text-center h5">{{$class->date_start}}</p></div>
                            <div class="col-md-1"><p class="text-center h5">-</p></div>
                            <div class="col-md-3"><p class="text-center h5">{{$class->date_end}}</p></div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2 mb-2">
                    <div class="col-md-12">
                        @foreach ($class->schedules as $day)
                            <div class="row">
                                <div class="col-md-2"><p class="text-center h6">{{$day->day}}</p></div>
                                <div class="col-md-2"><p class="text-center h6">{{$day->time_start}}</p></div>
                                <div class="col-md-2"><p class="text-center h6">{{$day->time_end}}</p></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                
               
               
                <div class="row">
                   <div class="ml-3 col-md-12">
                       <div class="row">
                           <div class="col-md-2">{{$class->price}} lei</div>
                           <div class="col-md-2">{{$class->seats}} seats</div>
                       </div>
                   </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12"><p class="h6">{{$class->description}}</p></div>
                </div>
                 
                  
               
           </div>
        </div>
    </div>
@endsection