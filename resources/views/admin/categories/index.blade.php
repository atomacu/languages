@extends('layouts.admin')

@section('content')
<div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="add-category-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-category-label">New category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form data-url="{{route('categories.store')}}" id="add-category-form" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-2 row justify-content-center">
                        <div class="col-md-10">
                           <input id="add-category-name" placeholder="Name" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="mb-2 row justify-content-center">
                        <div class="col-md-10">
                            <textarea placeholder="Description" class="form-control" id="add-category-description" required></textarea>
                        </div>
                    </div>

                    <select name="" id="">

                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                        <option value=""></option>
                    </select>
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <input type="file" class="form-control" name="category-img" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="add-category-btn" type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3"><i class="fas fa-folder"></i> Categories</p>
        </div>
        <div class="col-md-2 ml-auto">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add-category">
                    <i class="fas fa-folder-plus"></i>  Add category
            </button>
        </div>
    </div>
    <hr>
    <div class="row" id="categories">
        @foreach($categories->reverse() as $category)
        <div class="mb-2 col-md-3">
            <div class="card">
                <a href="{{route('categories.show',['id'=>$category->id])}}"><img class="category-card-img card-img-top" src="{{ asset('/images/categories/'.$category->img)}}" alt="Card image cap"></a>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <a href="{{route('categories.show',['id'=>$category->id])}}"> 
                                <h4 data-id="{{$category->id}}" class="category-title card-title">{{$category->name}}</h4>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="edit-category-button text-center btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-category-{{$category->id}}">
                                    <i class="far fa-edit"></i>
                            </button>
                            
                            <div class="modal fade" id="edit-category-{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="edit-category-{{$category->id}}-label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="edit-category-{{$category->id}}-label">Edit category</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                           
                                                <div class="modal-body">
                                                    <div class="mb-2 row justify-content-center">
                                                        <div class="col-md-10">
                                                            <input data-id="{{$category->id}}" placeholder="Name" value="{{$category->name}}" type="text" class="edit-category-name form-control">
                                                        </div>
                                                    </div>
                                                    <div class="mb-2 row justify-content-center">
                                                        <div class="col-md-10">
                                                            <textarea data-id="{{$category->id}}" placeholder="Description" class="edit-category-description form-control">{{$category->description}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-10">
                                                            <img style="width:100%;" src="{{ asset('/images/categories'.$category->img)}}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button data-url="{{route('categories.update',['id'=>$category->id])}}" data-id="{{$category->id}}" type="button" class="edit-category-btn btn btn-primary">Edit</button>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-md-10 ">
                            <a href="{{route('categories.show',['id'=>$category->id])}}">      
                                <p data-id="{{$category->id}}" class="category-description text-truncate card-text">{{$category->description}}</p>
                            </a>
                        </div>
                        <div class="col-md-2">
                            <button type="button" data-toggle="modal" data-target="#delete-category" class="btn btn-danger mr-auto btn-sm">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                           
                            <div class="modal fade" id="delete-category" tabindex="-1" role="dialog" aria-labelledby="delete-category-label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="delete-category-label">Delete category</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                       
                                        <div class="modal-body text-center">
                                            <p class="h5">Are you sure you want to delete <b>{{$category->name}}</b> ? </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button data-url="{{route('categories.destroy',['id'=>$category->id])}}" type="button" class="btn delete-category-btn btn-danger">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-4">
            {{ $categories->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
