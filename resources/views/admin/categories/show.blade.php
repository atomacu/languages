@extends('layouts.admin')

@section('content')
<div class="modal fade" id="add-class" tabindex="-1" role="dialog" aria-labelledby="add-class-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-class-label">Add class</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form autocomplete="off" data-id="{{$category->id}}" action="{{route('class.store')}}" method="POST" enctype="multipart/form-data" id='add-class-form'>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-md-11">
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-12">
                                    <select name="teacher" class="form-control" id="teacher" required>
                                        <option value="" selected disabled>Select teacher</option>
                                        @foreach ($teachers as $teacher)
                                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-12">
                                    <input id="class-name" class="form-control" placeholder="Name" type="text" required>
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-12">
                                    <textarea id="class-description" class="form-control" placeholder="Description"
                                        type="text" required></textarea>
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-12">
                                    <input class="btn col-md-12" name="classImg" type="file" required>
                                    <hr>
                                </div>
                            </div>

                            <div class="row mb-2 justify-content-center">

                                <div id="class-schedule" class="col-md-11 text-center">
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-md-6 p-0 pr-1">
                                            <input id="start-class-date" type="datepicker" class="form-control"
                                                placeholder="Start class date" required>
                                        </div>
                                        <div class="col-md-6 p-0 pl-1">
                                            <input id="end-class-date" type="datepicker" class="form-control"
                                                placeholder="End class date" required>
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Monday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="monday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="monday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Tuesday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="tuesday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="tuesday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Wednesday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="wednesday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="wednesday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Thursday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="thursday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="thursday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Friday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="friday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="friday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Saturday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="saturday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="saturday-2" type="time">
                                        </div>
                                    </div>
                                    <div class="mb-2 row">
                                        <div class="col-md-4 p-0">
                                            Sunday
                                        </div>
                                        <div class="col-md-4  p-0">
                                            <input class="form-control" name="week[]" id="sunday-1" type="time">
                                        </div>
                                        <div class="col-md-4 p-0">
                                            <input class="form-control" name="week[]" id="sunday-2" type="time">
                                        </div>
                                    </div>

                                    <hr>
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input id="class-seats" class="form-control" placeholder="Seats"
                                                type="number" required>
                                        </div>

                                        <div class="col-md-6">
                                            <input id="class-prices" class="form-control" placeholder="Price"
                                                type="number" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="add-class-btn" class="btn btn-primary">Add</button>
                    @csrf
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <p class="h3">{{$category->name}}</p>
            <p>{{$category->description}}</p>
        </div>
        <div class="ml-auto col-md-2">

            <button type="button" data-toggle="modal" data-target="#add-class" class="btn btn-primary btn-sm ml-auto">
                <i class="fas fa-plus"></i> Add class
            </button>
        </div>
    </div>
    <hr>
    <div class="row mt-3">
        <div class="col-md-4">
            {{ $classes->onEachSide(0)->links() }}
        </div>
    </div>
    <div id="classes-list" class="row">
        @foreach ($classes as $class)
        <div class="mb-2 col-md-4">
            <div class="card card-product">
                <div class="img-wrap">
                    <a href="{{route('class.show',['id'=>$class->id])}}"><img src="{{ asset('/images/classes/'.$class->img)}}"></a>
                </div>
                <div class="m-2 info-wrap">
                    <h4 class="title">{{$class->name}}</h4>
                    <p class="desc text-truncate">{{$class->description}}</p>
                    <div class="rating-wrap">
                        <div class="label-rating">{{$class->seats}} seats</div>
                    </div>
                </div>
                <div class="m-2 bottom-wrap">
                   
                    <a href="" data-toggle="modal" data-target="#edit-class-modal" class="ml-1 btn btn-sm btn-primary float-right">
                        <i class="far fa-edit"></i> Edit
                    </a>
                    <a href="{{route('class.show',['id'=>$class->id])}}" class="btn ml-1 btn-sm btn-warning float-right">
                        More details
                    </a>
                    <a href="#" data-toggle="modal" data-target="#delete-class-modal"  class="btn ml-1 btn-sm btn-danger float-right">
                        <i class="fas fa-trash-alt"></i>
                    </a>
                    <div class="price-wrap h5">
                        <span class="price-new">{{$class->price}} lei</span>
                    </div>

                    <div class="modal fade" id="delete-class-modal" tabindex="-1" role="dialog"
                    aria-labelledby="delete-class-modal-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-class-modal-label">Delete class</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="h5 text-center">Are you sure that you want to delete this class ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button data-url="{{ route('class.destroy',['id'=>$class->id]) }}" class="delete-class-btn btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>



                    <div class="modal fade" id="edit-class-modal" tabindex="-1" role="dialog"
                        aria-labelledby="edit-class-modal-label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-class-modal-label">Edit class</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" enctype="multipart/form-data" >
                                        <div class="modal-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-11">
                                                    <div class="row mb-2 justify-content-center">
                                                        <div class="col-md-12">
                                                            <select name="teacher" class="form-control" id="teacher">
                                                                <option value="" selected disabled>Select teacher
                                                                </option>
                                                                @foreach ($teachers as $teacher)
                                                                
                                                                <option value="{{$teacher->id}}" @if($teacher->id==$class->teacher_id) selected @endif>{{$teacher->name}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2 justify-content-center">
                                                        <div class="col-md-12">
                                                            <input id="class-name" class="form-control" value="{{$class->name}}" placeholder="Name" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2 justify-content-center">
                                                        <div class="col-md-12">
                                                            <textarea id="class-description" class="form-control" placeholder="Description" type="text">{{$class->description}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2 justify-content-center">
                                                        <div class="col-md-12">
                                                            <input class="btn col-md-12" name="classImg" type="file" multiple>
                                                            <hr>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-2 justify-content-center">

                                                        <div class="col-md-11 text-center">
                                                            <div class="row mb-2 justify-content-center">
                                                                <div class="col-md-6 p-0 pr-1">
                                                                    <input id="start-class-date" value="{{$class->date_start}}" type="datepicker" class="form-control" placeholder="Start class date">
                                                                </div>
                                                                <div class="col-md-6 p-0 pl-1">
                                                                    <input id="end-class-date" type="datepicker" value="{{$class->date_end}}" class="form-control" placeholder="End class date">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Monday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="monday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="monday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Tuesday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="tuesday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="tuesday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Wednesday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="wednesday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="wednesday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Thursday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="thursday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="thursday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Friday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="friday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="friday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Saturday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="saturday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="saturday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <div class="mb-2 row">
                                                                <div class="col-md-4 p-0">
                                                                    Sunday
                                                                </div>
                                                                <div class="col-md-4  p-0">
                                                                    <input class="form-control" id="sunday-1" type="time">
                                                                </div>
                                                                <div class="col-md-4 p-0">
                                                                    <input class="form-control" id="sunday-2" type="time">
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-2 justify-content-center">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input id="class-seats" class="form-control"
                                                                        placeholder="Seats" type="number">
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <input id="class-prices" class="form-control"
                                                                        placeholder="Price" type="number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                            @csrf
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection
