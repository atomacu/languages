@extends('layouts.guest')


@section('content')
<div class="vh-100 pt-5  mt-5 container">
    <div class="row  mt-5">
       <div id="class-image-show" class="col-md-5">
           <img src="{{ asset('/images/classes/'.$class->img)}}" alt="">
       </div>
       <div class="col-md-7">
            <div class="row"> 
                <div class="col-md-10">
                    <h3 class="m-0">
                        {{$class->name}}
                        <small class="h5 text-muted"><span class="mx-1" >{{$class->date_start}} </span><span class="mx-1"> {{$class->date_end}}</span></small>
                    </h3>
                </div>
            </div>
            <hr class="mt-0">
            <div class="row">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-5 ml-2"><p class="h5">{{$class->teacher->name}}</p></div>
                    </div>
                </div>
            </div>
            <div class="row mt-2 mb-2">
                <div class="col-md-12">
                    @foreach ($class->schedules as $day)
                        <div class="row">
                            <div class="col-md-2"><p class="text-center h6">{{$day->day}}</p></div>
                            <div class="col-md-2"><p class="text-center h6">{{$day->time_start}}</p></div>
                            <div class="col-md-2"><p class="text-center h6">{{$day->time_end}}</p></div>
                        </div>
                    @endforeach
                </div>
            </div>
            
           
    
            <div class="price-wrap h5 ml-2 mt-3">
                <span class="price-new mr-4">{{$class->price}} lei</span>
                <span class="price-new">{{$class->seats}} seats</span>
            </div>
                     
                 
            <div class="row">
               <div class="col-md-6">
                    <a href="" class="btn ml-1 btn-success">
                        <i class="fas fa-shopping-cart"></i> Buy
                    </a>

                    @if($class->exist==0) 
                        <button data-auth="{{Auth::user()? 1 : 0}}" data-id="{{$class->id}}" data-url="{{route('wishlist.store')}}" id="add-class-wishlist" class="btn ml-1 btn-danger">
                            <i class="fas fa-heart"></i> Wishlist
                        </button>
                    @endif
                </div>
            </div>
            
            <hr>
            <div class="row">
                <div class="col-md-12"><p class="h6">{{$class->description}}</p></div>
            </div>
             
              
           
       </div>
    </div>
</div>
@endsection