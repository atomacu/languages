@extends('layouts.guest')

@section('content')
<div class="vh-100 mt-5 pt-5 container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3"><i class="fas fa-folder"></i> Categories</p>
        </div>
    </div>
    <hr>
    <div class="row" id="categories">
        @foreach($categories->reverse() as $category)
        <div class="mb-2 col-md-3">
            <div class="card">
                <a href="{{route('clientCategories.show',['id'=>$category->id])}}"><img class="category-card-img card-img-top" src="{{ asset('/images/categories/'.$category->img)}}" alt="Card image cap"></a>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <a href="{{route('clientCategories.show',['id'=>$category->id])}}"> 
                                <h4 data-id="{{$category->id}}" class="category-title card-title">{{$category->name}}</h4>
                            </a>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-md-10 ">
                            <a href="{{route('clientCategories.show',['id'=>$category->id])}}">      
                                <p data-id="{{$category->id}}" class="category-description text-truncate card-text">{{$category->description}}</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="my-3 row">
        <div class="col-md-4">
            {{ $categories->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
