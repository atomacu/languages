@extends('layouts.guest')

@section('content')
<div class="vh-100 mt-5 pt-5 container">
    <div class="row">
        <div class="col-md-10">
            <p class="h3">{{$category->name}}</p>
            <p>{{$category->description}}</p>
        </div>
       
    </div>
    <hr>
    
    <div id="classes-list" class="row">
        @foreach ($classes as $class)
        <div class="mb-3 col-md-4">
            <div class="card card-product">
                <div class="img-wrap">
                    <a href="{{route('clientClasses.show',['id'=>$class->id])}}"><img src="{{ asset('/images/classes/'.$class->img)}}"></a>
                </div>
                <div class="m-2 info-wrap">
                    <h4 class="title">{{$class->name}}</h4>
                    <p class="desc text-truncate">{{$class->description}}</p>
                    <div class="rating-wrap">
                        <div class="label-rating">{{$class->seats}} seats</div>
                    </div>
                    <div class="price-wrap h5">
                        <span class="price-new">{{$class->price}} lei</span>
                    </div>
                </div>
                <div class="m-2 bottom-wrap">
                    <div class="container">
                        <div class="row">
                            @if($class->exist==0)    
                                <button data-auth="{{Auth::user()? 1 : 0}}" data-id="{{$class->id}}" data-url="{{route('wishlist.store')}}" id="add-class-wishlist" class="btn ml-1 my-1 col-md btn-sm btn-danger">
                                    <i class="fas fa-heart"></i> Wishlist
                                </button>
                            @endif
                            <a href="{{route('clientClasses.show',['id'=>$class->id])}}"
                                class="btn ml-1 my-1 col-md btn-sm btn-primary">
                                <i class="fas fa-info"></i> More details
                            </a>
                            <a href="" class="btn ml-1 col-md my-1 btn-sm btn-success">
                                <i class="fas fa-shopping-cart"></i> Buy
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row my-3">
        <div class="col-md-4">
            {{ $classes->onEachSide(0)->links() }}
        </div>
    </div>
</div>


@endsection
