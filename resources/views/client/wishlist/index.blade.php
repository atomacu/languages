@extends('layouts.client')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <p class="h3 m-0"><i class="fas fa-heart"></i> Wishlist</p>
        </div>
    </div>
    <hr>
    <div class="row mt-2">
        @foreach(Auth::user()->wishlist as $wishlist)
   
        <?php 
            $class=$wishlist->class
        ?>
        <div class="mb-3 col-md-4">
            <div class="card card-product">
                <div class="img-wrap">
                    <a href="{{route('clientClasses.show',['id'=>$class->id])}}"><img src="{{ asset('/images/classes/'.$class->img)}}"></a>
                </div>
                <div class="m-2 info-wrap">
                    <h4 class="title">{{$class->name}}</h4>
                    <p class="desc text-truncate">{{$class->description}}</p>
                    <div class="rating-wrap">
                        <div class="label-rating">{{$class->seats}} seats</div>
                        <div class="price-wrap h5">
                            <span class="price-new">{{$class->price}} lei</span>
                        </div>
                    </div>
                </div>
                <div class="m-2 bottom-wrap">
                   
                    <div class="container">
                        <div class="row">
                            <a href="{{route('clientClasses.show',['id'=>$class->id])}}" class="btn ml-1 my-1 col-md btn-sm btn-primary">
                                <i class="fas fa-info"></i> More details
                            </a>
                            <button data-url="{{route('wishlist.destroy',['id'=>$wishlist->id])}}" class="btn-wishlist-delete btn ml-1 my-1 col-md btn-sm btn-danger">
                                <i class="fas fa-trash"></i> Delete
                            </button>
                            <a href="" class="btn ml-1 col-md my-1 btn-sm btn-success">
                                <i class="fas fa-shopping-cart"></i> Buy
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
