$(document).ready(function() {
    $('#add-class-wishlist').click(function(){
        var auth=$(this).data('auth');
        var id=$(this).data('id');
        var url=$(this).data('url');

        if(auth==0){
            window.location.replace("/login");
        }

        $.ajax({
            type: 'POST',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: { _token : $('meta[name="csrf-token"]').attr('content'), 
                'id':id
            },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {      
            },
        });
        console.log();
    });
   
    $('.btn-wishlist-delete').click(function(){
        var url=$(this).data('url');

        $.ajax({
            type: 'DELETE',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: { _token : $('meta[name="csrf-token"]').attr('content'), 
            },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {      
            },
        });
    });
});



// $.ajax({
// 	type: 'get',
// 	dataType: 'json',
// 	url: '/activeDeactiveRow',
// 	data: {
//         'id': id,
//         "project":project 
//     },
// 	success: function (data) {
// 	},
//     async: false
// });



// $.ajax({
//     type: 'POST',
//     headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     },
//     url: "/changeStatus",
//     data: { _token : $('meta[name="csrf-token"]').attr('content'), 
//         'user':user,
//         'val':val
//     },
//     dataType: "text",
//     success: function(data) {
//     },
//     error: function(data) {      
//     },
// });


// $.ajax({
//     url:url,
//     method:"POST",
//     data:formData,
//     dataType:'JSON',
//     contentType: false,
//     cache: false,
//     processData: false,
//     success:function(data){
//         $('#category-file').val(null);
//     }
// });