$(document).ready(function() {

    $('#add-category-form').submit(function(event) {
        event.preventDefault();
        $('.alert-msg').remove();
        var url = $(this).data('url');

        var formData = new FormData(this);
        var name = $('#add-category-name').val();
        var description = $('#add-category-description').val();
        formData.append('name', name);
        formData.append('description', description);
        var check = 0;

        $('#add-category-btn').attr('disabled', true);

        if (name == "") {
            $('#add-category-name').after('<p class="alert-msg">Name field is empty</p>');
        } else {
            check++;
        }

        if (description == "") {
            $('#add-category-description').after('<p class="alert-msg">Description field is empty</p>');
        } else {
            check++;
        }

        if (check == 2) {
            $.ajax({
                url: url,
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    location.reload();
                }
            });
        }

    });

    $('.edit-category-btn').click(function() {
        var id = $(this).data('id');
        var url = $(this).data('url');
        var name;
        var description;
        $(".edit-category-name").each(function() {
            if ($(this).data("id") == id) {
                name = $(this).val();
            }
        });

        $(".edit-category-description").each(function() {
            if ($(this).data("id") == id) {
                description = $(this).val();
            }
        });
        $(this).attr('disabled', true);
        console.log(name, description);
        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                'name': name,
                'description': description
            },
            dataType: "text",
            success: function(data) {
                console.log(data);
                location.reload();
            },
            error: function(data) {

            }
        });
    });

    $('.delete-category-btn').click(function() {
        var url = $(this).data('url');
        $(this).attr('disabled', true);
        $.ajax({
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: { _token: $('meta[name="csrf-token"]').attr('content'), },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {

            },
        });
    });

    $('#add-teacher-form').submit(function(event) {
        event.preventDefault();
        var formData = new FormData(this);
        var name = $('#add-teacher-name').val();
        var description = $('#add-teacher-description').val();
        var url = $(this).data('url');

        formData.append('name', name);
        formData.append('description', description);

        $('#add-teacher-btn').attr('disabled', true);

        $.ajax({
            url: url,
            method: "POST",
            data: formData,
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                location.reload();
            }
        });
    });

    $('.edit-teacher-btn').click(function() {
        var id = $(this).data('id');
        var url = $(this).data('url');
        var name = $('.edit-teacher-name').val();
        var description = $('.edit-teacher-description').val();


        $(this).attr('disabled', true);

        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                'id': id,
                'name': name,
                'description': description
            },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {

            },
        });
        console.log(name, description);
    });


    $('.delete-teacher-btn').click(function() {
        var url = $(this).data('url');
        $(this).attr('disabled', true);
        $.ajax({
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: { _token: $('meta[name="csrf-token"]').attr('content'), },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {

            },
        });
    });



    $('#add-class-form').submit(function(event) {
        event.preventDefault();
        var url = $(this).attr('action');
        var formData = new FormData(this);
        var catId = $(this).data('id');
        var teacherId = $("#teacher option:selected").val();
        var className = $('#class-name').val();
        var classDescription = $('#class-description').val();
        var startDateClass = $('#start-class-date').val();
        var endDateClass = $('#end-class-date').val();
        var seats = $('#class-seats').val();
        var price = $('#class-prices').val();
        var checkSchedule=0;

        var monday = [($('#monday-1').val() != "") && ($('#monday-2').val() != "") ? 1 : 0, $('#monday-1').val(), $('#monday-2').val()];
        var tuesday = [($('#tuesday-1').val() != "") && ($('#tuesday-2').val() != "") ? 1 : 0, $('#tuesday-1').val(), $('#tuesday-2').val()];
        var wednesday = [($('#wednesday-1').val() != "") && ($('#wednesday-2').val() != "") ? 1 : 0, $('#wednesday-1').val(), $('#wednesday-2').val()];
        var thursday = [($('#thursday-1').val() != "") && ($('#thursday-2').val() != "") ? 1 : 0, $('#thursday-1').val(), $('#thursday-2').val()];
        var friday = [($('#friday-1').val() != "") && ($('#friday-2').val() != "") ? 1 : 0, $('#friday-1').val(), $('#friday-2').val()];
        var saturday = [($('#saturday-1').val() != "") && ($('#saturday-2').val() != "") ? 1 : 0, $('#saturday-1').val(), $('#saturday-2').val()];
        var sunday = [($('#sunday-1').val() != "") && ($('#sunday-2').val() != "") ? 1 : 0, $('#sunday-1').val(), $('#sunday-2').val()];

        $('.alert').remove();
        if(monday[0]==0 && tuesday[0]==0 && wednesday[0]==0 && thursday[0]==0 && friday[0]==0 && saturday[0]==0 && sunday[0]==0){
          $('#class-schedule').append('<p class="alert" style="color:red; text-align:center;">Complete the course schedule for at least one day</p>');  // 
        }else{
            checkSchedule=1;
        }


        formData.append('monday', monday);
        formData.append('tuesday', tuesday);
        formData.append('wednesday', wednesday);
        formData.append('thursday', thursday);
        formData.append('friday', friday);
        formData.append('saturday', saturday);
        formData.append('sunday', sunday);
        formData.append('teacherId', teacherId);
        formData.append('className', className);
        formData.append('classDescription', classDescription);
        formData.append('startDateClass', startDateClass);
        formData.append('endDateClass', endDateClass);
        formData.append('seats', seats);
        formData.append('price', price);
        formData.append('catId', catId);

        

        if(checkSchedule==1){
            $('#add-class-btn').attr('disabled', true);
            $.ajax({
                url: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    location.reload();
                }
            });
        }

    });

    $("[type='datepicker']").datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        orientation: 'bottom auto',
        todayHighlight: true,
        startDate: new Date(),
    });

    $('.delete-class-btn').click(function() {
        var url = $(this).data('url');
        $(this).attr('disabled', true);
        $.ajax({
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: { _token: $('meta[name="csrf-token"]').attr('content'), },
            dataType: "text",
            success: function(data) {
                location.reload();
            },
            error: function(data) {
            },
        });
    });



});



// $.ajax({
// 	type: 'get',
// 	dataType: 'json',
// 	url: '/activeDeactiveRow',
// 	data: {
//         'id': id,
//         "project":project 
//     },
// 	success: function (data) {
// 	},
//     async: false
// });



// $.ajax({
//     type: 'POST',
//     headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     },
//     url: "/changeStatus",
//     data: { _token : $('meta[name="csrf-token"]').attr('content'), 
//         'user':user,
//         'val':val
//     },
//     dataType: "text",
//     success: function(data) {
//     },
//     error: function(data) {      
//     },
// });


// $.ajax({
//     url:url,
//     method:"POST",
//     data:formData,
//     dataType:'JSON',
//     contentType: false,
//     cache: false,
//     processData: false,
//     success:function(data){
//         $('#category-file').val(null);
//     }
// });