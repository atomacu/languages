<?php

use Illuminate\Support\Facades\DB;
use App\Roles;
use App\UsersRoles;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Roles::store('Admin',"Poate face tot ce vrea.",1,'admin');
        $roleId=Roles::store('Client',"Nu poate face tot ce vrea.",1,'client',1);

        $userId=DB::table('users')->insertGetId([
            'name'=>"Tomacu Alexandru",
            'email'=>"atomacu@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);
        UsersRoles::store($userId,$roleId);
    }
}
