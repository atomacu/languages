<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['roles']], function(){

    Route::get('/', 'WellcomeController@wellcome')->name('wellcome');
    
    Auth::routes();
    
    Route::resource('admin','Admin\AdminController');
    Route::resource('security','Admin\SecurityController');
    Route::resource('pages','Admin\PagesController');
    Route::resource('roles','Admin\RolesController');
    Route::resource('categories','Admin\CategoriesController');
    Route::resource('teachers','Admin\TeachersController');
    Route::resource('schedule','Admin\ScheduleController');
    Route::resource('class','Admin\ClassController');




    Route::resource('client','Client\ClientController');
    Route::resource('clientCategories','Client\CategoriesController');
    Route::resource('clientClasses','Client\ClassController');
    Route::resource('wishlist','Client\WishlistController');
    
    Route::get('/home', 'HomeController@index')->name('home');
    
});

