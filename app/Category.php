<?php

namespace App;

use Auth;
use App\ClassModel;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const table = 'categories';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','description','img','user_id'
    ];

    static function store($name,$description){
        return parent::create([
            'user_id'=>Auth::user()->id,
            'name'=>$name,
            'description'=>$description,
        ])->id;
    }

    static function setImg($id,$img){
        parent::where('id',$id)->update([
            'img'=>$img
        ]);

        return 1; 
    }

    static function allPaginate($nr){
        return parent::where('visibility',1)->paginate($nr);
    }

    static function getClasses($id){
        return ClassModel::where('cat_id',$id)->where('visibility',1)->paginate(6);
    }

    static function destroy($id){
        parent::where('id',$id)->update([
            'visibility'=>0
        ]);
        return 1;
    }
    
    public function classes(){
        return $this->hasMany('App\ClassModel',"cat_id");
    }
}
