<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    const table = 'wishlists';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id','class_id'
    ];

    static function store($id){
        if(self::inList($id)==0){
            return parent::create([
                'user_id'=>Auth::user()->id,
                'class_id'=>$id
            ])->id;
        } else return "error";
      
    }

    static function inList($id){
        if(Auth::user()){
            $exist=parent::where('class_id',$id)->where('user_id',Auth::user()->id)->first();
            $return=$exist ? 1 : 0;
            return $return;
        } else return 0;
       
    }

    public function class(){
        return $this->belongsTo('App\ClassModel','class_id');
    }
}
