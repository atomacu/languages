<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRoles extends Model
{
    const table = 'users_roles';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id','role_id'
    ];

    static function store($userId,$roleId){
        parent::create([
            'user_id'=>$userId,
            'role_id'=>$roleId
        ]);
    }

    public function role(){
        return $this->belongsTo('App\Roles','role_id');
    }
}
