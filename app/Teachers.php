<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    const table = 'teachers';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','description','img'
    ];

    static function store($name,$description){
        return parent::create([
            'name'=>$name,
            'description'=>$description
        ])->id;
    }

    static function setImg($id,$img){
        parent::where('id',$id)->update([
            'img'=>$img
        ]);
        return 1;
    }
    
    static function allPaginate($nr){
        return parent::where('visibility',1)->paginate($nr);
    }

    static function destroy($id){
        parent::where('id',$id)->update([
            'visibility'=>0
        ]);
        return 1;
    }

    static function getTeachers(){
        return parent::where('visibility',1)->get();
    }

    public function classes(){
        return $this->hasMany('App\ClassModel',"teacher_id");
    }
}
