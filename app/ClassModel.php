<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class ClassModel extends Model
{
    const table = 'classes';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'cat_id','teacher_id','name','description','price','seats','img','date_start','date_end'
    ];

    static function store($catId,$teacherId,$name,$description,$price,$seats,$date_start,$date_end){
        return parent::create([
            'cat_id'=>$catId,
            'teacher_id'=>$teacherId,
            'name'=>$name,
            'description'=>$description,
            'price'=>$price,
            'seats'=>$seats,
            'date_start'=>$date_start,
            'date_end'=>$date_end
        ])->id;
    }

    static function editImg($id,$name){
        parent::where('id',$id)->update([
            'img'=>$name
        ]);
        return 1;
    }

    static function destroy($id){
        $id=(int) $id;
        $class=parent::find($id);
        unlink('./images/classes/'.$class->img);
        // dump($id);
        parent::where('id',$id)->update([
            'visibility'=>0
            ]);
        return 1;
    }

    public function schedules(){
        return $this->hasMany('App\Schedule',"class_id");
    }

    public function teacher(){
        return $this->belongsTo('App\Teachers');
    }
  
}