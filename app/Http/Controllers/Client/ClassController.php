<?php

namespace App\Http\Controllers\Client;

use Auth;
use App\WishList;
use App\Category;
use App\ClassModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassController extends Controller
{
    public function show($id){
        $class=ClassModel::find($id);
        $categoriesAll=Category::all();

        $class->exist=WishList::inList($id);
        return view('client.categories.classes.show',['class'=>$class,'categoriesAll'=>$categoriesAll]);
    }
}
