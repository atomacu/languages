<?php

namespace App\Http\Controllers\Client;

use App\Category;
use App\WishList;
use App\ClassModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
   

    public function index(){
        $categories=Category::allPaginate(8);
        $categoriesAll=Category::all();
        return view('client.categories.index',['categories'=>$categories,'categoriesAll'=>$categoriesAll]);
    }

    public function show($id){
        $category=Category::find($id);
        $classes=Category::getClasses($id);
        $categoriesAll=Category::all();

        foreach ($classes as $i => $class) {
            $class->exist=WishList::inList($class->id);
        }

        return view('client.categories.show',['category'=>$category,'classes'=>$classes,'categoriesAll'=>$categoriesAll]);
    }
}
