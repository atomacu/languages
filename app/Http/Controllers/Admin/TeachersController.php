<?php

namespace App\Http\Controllers\Admin;

use App\Teachers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $teachers=Teachers::allPaginate(8);
        return view('admin.teachers.index',['teachers'=>$teachers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teacher-img' => ['required', 'image', 'max:2048']
        ]);

        if ($validator->passes()) {
          


            $id=Teachers::store($request['name'],$request['description']);

            $image = $request->file('teacher-img');
            $new_name = 'teacher-'.$id.'.'. $image->getClientOriginalExtension();
            $image->move('./images/teachers', $new_name);

            Teachers::setImg($id,$new_name);

            return 1;
        }else{
            return response()->json([
                'errors' => $validator->errors()->all()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Teachers::where('id',$id)->update([
            'name'=>$request['name'],
            'description'=>$request['description']
        ]);
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Teachers::destroy($id);
    }
}
