<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Teachers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $categories=Category::allPaginate(8);
        return view('admin.categories.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category-img' => ['required', 'image', 'max:2048']
        ]);

        if ($validator->passes()) {
            $id=Category::store($request['name'],$request['description']);
            $image = $request->file('category-img');
            $new_name = 'category-'.$id.'.'. $image->getClientOriginalExtension();
            $image->move('./images/categories', $new_name);
            Category::setImg($id,$new_name);
            return 1;
        }else{
            return response()->json([
                'errors' => $validator->errors()->all()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $classes=Category::find($id)->classes()->where('visibility',1)->paginate(6);
       $category=Category::find($id);
       $teachers=Teachers::getTeachers();
       return view('admin.categories.show',['category'=>$category,'teachers'=>$teachers,'classes'=>$classes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Category::where('id',$id)->update([
            'name'=>$request['name'],
            'description'=>$request['description']
            ]);
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Category::destroy($id);
    }
}
