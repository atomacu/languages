<?php

namespace App\Http\Controllers\Admin;

use App\Schedule;
use App\ClassModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'classImg' => ['required', 'image', 'max:2048']
        ]);
           
        
            $classId=ClassModel::store($request['catId'],$request['teacherId'],$request['className'],$request['classDescription'],$request['price'],$request['seats'],$request['startDateClass'],$request['endDateClass']);
            $image = $request->file('classImg');
          
            $new_name = 'class-'.$classId.'.'. $image->getClientOriginalExtension();
            $image->move('./images/classes', $new_name);
    
            ClassModel::editImg($classId,$new_name);
       



        $monday=explode(",",$request['monday']);
       
        if($monday[0]==1){
            Schedule::store($classId,"Monday",$monday[1],$monday[2]);
        }
        
        $tuesday=explode(",",$request['tuesday']);
     
        if($tuesday[0]==1){
            Schedule::store($classId,"Tuesday",$tuesday[1],$tuesday[2]);
        }
       
        $wednesday=explode(",",$request['wednesday']);
       
        if($wednesday[0]==1){
            Schedule::store($classId,"Wednesday",$wednesday[1],$wednesday[2]);
        }
        $thursday=explode(",",$request['thursday']);
       
        if($thursday[0]==1){
            Schedule::store($classId,"Thursday",$thursday[1],$thursday[2]);
        }
        $friday=explode(",",$request['friday']);
        
        if($friday[0]==1){
            Schedule::store($classId,"Friday",$friday[1],$friday[2]);
        }
        $saturday=explode(",",$request['saturday']);
       
        if($saturday[0]==1){
            Schedule::store($classId,"Saturday",$saturday[1],$saturday[2]);
        }
        $sunday=explode(",",$request['sunday']);
       
        if($sunday[0]==1){
            Schedule::store($classId,"Sunday",$sunday[1],$sunday[2]);
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class=ClassModel::find($id);   
        return view('admin.categories.classes.show',['class'=>$class]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ClassModel::destroy($id);
    }
}
