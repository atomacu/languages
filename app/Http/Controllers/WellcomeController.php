<?php

namespace App\Http\Controllers;

use App\Category;
use App\WishList;
use App\ClassModel;
use Illuminate\Http\Request;

class WellcomeController extends Controller
{
    public function wellcome(){
        $categories=Category::where("visibility",1)->inRandomOrder()->take(3)->get();
        $categoriesAll=Category::where("visibility",1)->get();
        $classes=ClassModel::where("visibility",1)->inRandomOrder()->take(4)->get();

        foreach ($classes as $i => $class) {
            $class->exist=WishList::inList($class->id);
        }
        
 
        return view('welcome',['categories'=>$categories,'classes'=>$classes,'categoriesAll'=>$categoriesAll]);
    }
        
}
