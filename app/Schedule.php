<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    const table = 'schedules';
    protected $table = self::table;
    protected $primaryKey = 'id';

    protected $fillable = [
        'class_id','day','time_start','time_end'
    ];
    
    static function store($classId,$day,$time_start,$time_end){
        parent::create([
            'class_id'=>$classId,
            'day'=>$day,
            'time_start'=>$time_start,
            'time_end'=>$time_end
        ]);
    }
}
